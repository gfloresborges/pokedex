package br.com.topsafe.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.topsafe.Entity.Corretor;
import br.com.topsafe.Entity.ServicoContratado;
import br.com.topsafe.Services.CorretorService;
import br.com.topsafe.Services.ServicoContratadoService;

@Controller
@RequestMapping("/api/servico/contratados")
public class ServicoContratadoController {
	
	@Autowired
	ServicoContratadoService servicoContratadoService;
	
	@Autowired
	CorretorService corretorService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<ServicoContratado> listarTodos() {
		return servicoContratadoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public ServicoContratado buscarPorID(@PathVariable Integer id) {
		return servicoContratadoService.buscarPorID(id);		
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public ServicoContratado salvarNovo(@RequestBody ServicoContratado servicoContratado) {
		Corretor corretor = corretorService.buscarPorID(servicoContratado.getCorretor().getId());
		corretor.setComissao(corretor.getComissao() + servicoContratado.getValor() * 0.1);
		servicoContratado.setCorretor(corretor);
		return servicoContratadoService.salvar(servicoContratado);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public ServicoContratado editar(@PathVariable Integer id, @RequestBody ServicoContratado servicoContratado) {
		return servicoContratadoService.editar(id, servicoContratado);
	}
}
