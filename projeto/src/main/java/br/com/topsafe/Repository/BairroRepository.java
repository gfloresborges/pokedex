package br.com.topsafe.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.topsafe.Entity.Bairro;
import br.com.topsafe.Entity.Cidade;

public interface BairroRepository extends CrudRepository<Bairro, Integer>{
	Bairro findByNome(String nome);
	List<Bairro> findAllByCidade(Cidade cidade);
}
