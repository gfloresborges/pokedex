package br.com.topsafe.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.topsafe.Entity.Cidade;
import br.com.topsafe.Entity.Estado;

public interface CidadeRepository extends CrudRepository<Cidade, Integer>{
	Cidade findByNome(String nome);
	List<Cidade> findAllByEstado(Estado estado);
}
