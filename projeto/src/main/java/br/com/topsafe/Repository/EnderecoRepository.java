package br.com.topsafe.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.topsafe.Entity.Bairro;
import br.com.topsafe.Entity.Endereco;
import br.com.topsafe.Entity.Pessoa;

public interface EnderecoRepository<E extends Endereco> extends CrudRepository<E, Integer>{
	List<E> findAllByLogradouro(String logradouro);
	List<E> findAllByNumero(Integer numero);
	List<E> findAllByBairro(Bairro bairro);
	List<E> findAllByPessoas(Pessoa pessoa);
}
