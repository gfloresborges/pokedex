package br.com.topsafe.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.topsafe.Entity.Estado;

public interface EstadoRepository extends CrudRepository<Estado, Integer>{
	Estado findByNome(String nome);
}
