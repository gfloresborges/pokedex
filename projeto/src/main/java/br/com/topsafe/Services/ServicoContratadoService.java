package br.com.topsafe.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.topsafe.Entity.Segurado;
import br.com.topsafe.Entity.ServicoContratado;
import br.com.topsafe.Repository.SeguradoRepository;
import br.com.topsafe.Repository.ServicoContratadoRepository;

@Service
public class ServicoContratadoService {

	@Autowired
	public ServicoContratadoRepository servicoContratadoRepository;
	
	@Autowired
	public SeguradoRepository seguradoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado salvar(ServicoContratado servicoContratado) {
		Segurado segurado = seguradoRepository.findById(servicoContratado.getSegurado().getId()).get();
		segurado.setQtdServicos(segurado.getQtdServicos() + 1);
		servicoContratado.setSegurado(segurado);
		return servicoContratadoRepository.save(servicoContratado);
	}
	
	public ServicoContratado buscarPorID(Integer id) {
		if( servicoContratadoRepository.findById(id).isPresent() )
			return servicoContratadoRepository.findById(id).get();
		return null;
	}
	
	public List<ServicoContratado> buscarTodos() {
		return (List<ServicoContratado>) servicoContratadoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado editar(Integer id, ServicoContratado servicoContratado) {
		servicoContratado.setId(id);
		return servicoContratadoRepository.save(servicoContratado);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(ServicoContratado servicoContratado) {
		servicoContratadoRepository.delete(servicoContratado);
	}
	
}
