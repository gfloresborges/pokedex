package br.com.dbc.lotr;

import br.com.dbc.lotr.entity.Contato;
import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.ElfoPerClass;
import br.com.dbc.lotr.entity.ElfoTabelao;
import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.HobbitPerClass;
import br.com.dbc.lotr.entity.HobbitTabelao;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.TipoContato;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.EnderecoDTO;
import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.dto.UsuarioPersonagemDTO;
import br.com.dbc.lotr.service.UsuarioService;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UsuarioService usuarioService = new UsuarioService();
        UsuarioPersonagemDTO dto = new UsuarioPersonagemDTO();
        dto.setApelidoUsuario("Gui");
        dto.setNomeUsuario("Guilherme");
        dto.setCpfUsuario(123l);
        dto.setSenhaUsuario("123");
        
        EnderecoDTO enderecoDTO = new EnderecoDTO();
        enderecoDTO.setLogradouro("Rua Andarai");
        enderecoDTO.setNumero(531);
        enderecoDTO.setBairro("Passo da Areia");
        enderecoDTO.setCidade("Porto Alegre");
        enderecoDTO.setComplemento("CEP 300");
        dto.setEnderecoUsuario(enderecoDTO);
        
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setNome("Legolas");
        personagemDTO.setDanoElfo(100d);
        dto.setPersonagem(personagemDTO);
        
        usuarioService.cadastrarUsuarioEPersonagem(dto);
    
        System.exit(0);
                
    }
    
    
    public static void oldMain(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSession();
            transaction = session.beginTransaction();

            Usuario usuario = new Usuario();
            usuario.setNome("Antônio");
            usuario.setApelido("Antônio");
            usuario.setCpf(123l);
            usuario.setSenha("456");
            
            Endereco endereco = new Endereco();
            endereco.setLogradouro("Rua Andarai");
            endereco.setNumero(531);
            endereco.setBairro("Passo da Areia");
            endereco.setComplemento("predio comercial");
            endereco.setCidade("Porto Alegre");
            
            Endereco endereco2 = new Endereco();
            endereco2.setLogradouro("Rua Andarai");
            endereco2.setNumero(100);
            endereco2.setBairro("Passo da Areia");
            endereco2.setComplemento("predio comercial");
            endereco2.setCidade("Porto Alegre");
            
            TipoContato tipoContato = new TipoContato();
            tipoContato.setNome("Celular");
            tipoContato.setQuantidade(4);
            
            Contato contato = new Contato();
            contato.setTipoContato(tipoContato);
            contato.setUsuario(usuario);
            contato.setValor("99999999");
            
            tipoContato.setContato(contato);
            usuario.pushContatos(contato);
            usuario.pushEnderecos(endereco, endereco2);

            session.save(usuario);
            
            ElfoTabelao elfoTabelao = new ElfoTabelao();
            elfoTabelao.setDanoElfo(100d);
            elfoTabelao.setNome("Legolas");
            session.save(elfoTabelao);
            
            HobbitTabelao hobbitTabelao = new HobbitTabelao();
            hobbitTabelao.setDanoHobbit(10d);
            hobbitTabelao.setNome("Frodo Bonceiro");
            session.save(hobbitTabelao);
            
            ElfoPerClass elfoPerClass = new ElfoPerClass();
            elfoPerClass.setNome("Legolas Per Class");
            elfoPerClass.setDanoElfo(100d);
            session.save(elfoPerClass);                       
            
            HobbitPerClass hobbitPerClass = new HobbitPerClass();
            hobbitPerClass.setNome("Frodo Per Class");
            hobbitPerClass.setDanoHobbit(10d);
            session.save(hobbitPerClass);

            ElfoJoin elfoJoin = new ElfoJoin();
            elfoJoin.setNome("Legolas Join");
            elfoJoin.setDanoElfo(100d);
            session.save(elfoJoin);                       
            
            HobbitJoin hobbitJoin = new HobbitJoin();
            hobbitJoin.setNome("Frodo Join");
            hobbitJoin.setDanoHobbit(10d);
            session.save(hobbitJoin);
            
            //Consulta usando Criteria
            Criteria criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            /*restrictions cria um filtro que retorna os objetos que atendem a
            restrição. ilike compara string ignorando caixa textual            
            */
            criteria.add(Restrictions.and(
                    Restrictions.ilike("endereco.bairro", "%da areia"),
                    Restrictions.ilike("endereco.cidade", "%alegre")));
            /*criteria.list().forEach(System.out::println);
            operação funcional, semelhante a uma arrow function do javascript*/
            criteria.list().forEach((x) -> System.out.println(x));
            
            criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions.and(
                    Restrictions.ilike("endereco.bairro", "%da areia"),
                    Restrictions.ilike("endereco.cidade", "%alegre")));
            
            /*projection equivale ao select, ou seja definir o que pesquisar
            criteria.setProjection(Projections.rowCount());
                conta as linhas     */
            criteria.setProjection(Projections.count("id"));
            System.out.println(String.format("Foram encontrados %s registro(s) "
            + "com os critérios especificados", criteria.uniqueResult()));
            
            //Consulta usando HQL
            session.createQuery("select u from Usuario u "
                    + "join u.enderecos endereco "
                    + "where lower(endereco.cidade) like '%alegre' "
                    + "and lower(endereco.bairro) like '%da areia' "                    
                    ).list().forEach(System.out::println);
            
            Long count = (Long)session
                    .createQuery("select count(distinct u.id) from Usuario u "
                    + "join u.enderecos endereco "
                    + "where lower(endereco.cidade) like '%alegre' "
                    + "and lower(endereco.bairro) like '%da areia' " ).uniqueResult();
            System.out.println(String.format("Contamos com HQL %s usuarios", count));
            
            count = (Long)session
                    .createQuery("select count(endereco.id) from Usuario u "
                    + "join u.enderecos endereco "
                    + "where lower(endereco.cidade) like '%alegre' "
                    + "and lower(endereco.bairro) like '%da areia' " ).uniqueResult();
            System.out.println(String.format("Contamos com HQL %s enderecos", count));
            
            Long sum = (Long)session
                    .createQuery("select sum(endereco.numero) from Usuario u "
                    + "join u.enderecos endereco "
                    + "where lower(endereco.cidade) like '%alegre' "
                    + "and lower(endereco.bairro) like '%da areia' " ).uniqueResult();
            System.out.println(String.format("Total da soma dos numeros dos enderecos com HQL foi %s ", sum));
            
            transaction.commit();
        } catch (Exception ex) {
            if(transaction != null)
                transaction.rollback();
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            System.exit(1);
        } finally {
            if(session != null)
                session.close();            
        }
        System.exit(0);

    }

}
