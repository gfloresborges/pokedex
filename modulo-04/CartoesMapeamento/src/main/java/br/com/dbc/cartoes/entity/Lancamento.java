/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.cartoes.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author guilherme.borges
 */
@Entity
@Table(name = "LANCAMENTO")
public class Lancamento {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "lancamento_seq", sequenceName = "lancamento_seq")
    @GeneratedValue(generator = "lancamento_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "ID_CARTAO")
    private Cartao cartao;
    
    @ManyToOne
    @JoinColumn(name = "ID_LOJA")
    private Loja loja;
    
    @ManyToOne
    @JoinColumn(name = "ID_CREDENCIADOR")
    private Credenciador credenciador;
    
    private String descricao;
    
    private Double valor;
    
    @Column(name = "DATA_COMPRA")
    private String dataCompra;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getDataCompra() {
        return dataCompra;
    }

    public void setDataCompra(String dataCompra) {
        this.dataCompra = dataCompra;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public Credenciador getCredenciador() {
        return credenciador;
    }

    public void setCredenciador(Credenciador credenciador) {
        this.credenciador = credenciador;
    }    
    
}
