/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.digital.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "CLIENTE")
public class Cliente {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "cliente_seq", sequenceName = "cliente_seq")
    @GeneratedValue(generator = "cliente_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String nome;
    
    private String cpf;
    
    private String rg;
    
    @Column(name = "CREDITO_DISPONIVEL")
    private Integer creditoDisponivel;
    
    @ManyToMany(mappedBy = "clientes")
    private List<Conta> contas = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Integer getCreditoDisponivel() {
        return creditoDisponivel;
    }

    public void setCreditoDisponivel(Integer creditoDisponivel) {
        this.creditoDisponivel = creditoDisponivel;
    }

    public List<Conta> getContas() {
        return contas;
    }

    public void pushContas(Conta... contas) {
        this.contas.addAll(Arrays.asList(contas));
    }
    
    
}
