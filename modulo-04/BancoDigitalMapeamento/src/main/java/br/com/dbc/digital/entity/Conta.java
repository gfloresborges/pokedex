/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.digital.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "CONTA")
public class Conta {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "conta_seq", sequenceName = "conta_seq")
    @GeneratedValue(generator = "conta_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "ID_AGENCIA")
    private Agencia agencia;
    
    private Integer numero;
    
    private Integer saldo;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CONTA_CLIENTE", joinColumns = {
        @JoinColumn(name = "ID_CONTA")}, inverseJoinColumns = {
            @JoinColumn(name = "ID_CLIENTE")})
    private List<Cliente> clientes = new ArrayList<>();
    
    @OneToMany(mappedBy = "contaOrigem", cascade = CascadeType.ALL)
    private List<Lancamento> lancamentosRealizados = new ArrayList<>();
    
    @OneToMany(mappedBy = "contaDestino", cascade = CascadeType.ALL)
    private List<Lancamento> lancamentosRecebidos = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getSaldo() {
        return saldo;
    }

    public void setSaldo(Integer saldo) {
        this.saldo = saldo;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void pushClientes(Cliente... clientes) {
        this.clientes.addAll(Arrays.asList(clientes));
    }

    public List<Lancamento> getLancamentosRealizados() {
        return lancamentosRealizados;
    }

    public void pushLancamentosRealizados(Lancamento... lancamentosRealizados) {
        this.lancamentosRealizados.addAll(Arrays.asList(lancamentosRealizados));
    }

    public List<Lancamento> getLancamentosRecebidos() {
        return lancamentosRecebidos;
    }

    public void pushLancamentosRecebidos(Lancamento... lancamentosRecebidos) {
        this.lancamentosRecebidos.addAll(Arrays.asList(lancamentosRecebidos));
    }
    
    
}
