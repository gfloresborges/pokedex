/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.digital.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "TIPO_LANCAMENTO")
public class TipoLancamento {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "tipo_lancamento_seq", sequenceName = "tipo_lancamento_seq")
    @GeneratedValue(generator = "tipo_lancamento_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String nome;
    
    private Integer codigo;
    
    @OneToMany(mappedBy = "tipoLancamento", cascade = CascadeType.ALL)
    private List<Lancamento> lancamentos = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public List<Lancamento> getLancamentos() {
        return lancamentos;
    }

    public void pushLancamentos(Lancamento... lancamentos) {
        this.lancamentos.addAll(Arrays.asList(lancamentos));
    }
        
    
}
