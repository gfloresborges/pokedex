// eslint-disable-line no-unused-vars
const pokeApi = new PokeApi();
let idAtual;
let idSorteadas = [];
if ( localStorage.length > 0 ) {
  idSorteadas = JSON.parse( localStorage.getItem( 'idStorage' ) );
}

function montarLista( arr ) {
  const ul = document.createElement( 'ul' );
  arr.forEach( item => {
    const li = document.createElement( 'li' );
    const tipo = document.createTextNode( `${ item }` );
    li.appendChild( tipo );
    ul.appendChild( li );
  } )
  return ul;
}

function substituirLista( div, novaLista ) {
  div.removeChild( div.querySelector( 'ul' ) );
  div.appendChild( novaLista );
  return div;
}

function renderizaPokemonNaTela( pokemon ) {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );
  dadosPokemon.querySelector( '.nome' ).innerText = `Nome : ${ pokemon.nome }`;
  dadosPokemon.querySelector( '.thumb' ).src = pokemon.thumUrl;
  dadosPokemon.querySelector( '.id' ).innerText = `ID : ${ pokemon.id }`;
  dadosPokemon.querySelector( '.altura' ).innerText = `Altura : ${ pokemon.altura } cm`;
  dadosPokemon.querySelector( '.peso' ).innerText = `Peso : ${ pokemon.peso } kg`;
  substituirLista( dadosPokemon.querySelector( '.tipos' ), montarLista( pokemon.tipos ) );
  substituirLista( dadosPokemon.querySelector( '.estatisticas' ), montarLista( pokemon.estatisticas ) );
}

function requisitar( id ) {
  if ( id !== idAtual ) {
    pokeApi.buscar( id ).then( pokemonServidor => {
      const poke = new Pokemon( pokemonServidor );
      renderizaPokemonNaTela( poke );
      idAtual = id;
    } )
  }
}

function pesquisarPorNumero( evento ) {
  const id = evento.target.value;
  const idValido = pokeApi.validarID( id );
  if ( idValido ) {
    requisitar( id );
  } else {
    alert( 'Digite um id válido' ); // eslint-disable-line no-alert
  }
}

function pesquisarPorClick() {
  let id = pokeApi.sortearID();
  while ( ( !pokeApi.validarID( id ) ) || idSorteadas.includes( id ) ) {
    id = pokeApi.sortearID();
  }
  document.getElementById( 'campoPesquisar' ).querySelector( 'input' ).value = '';
  idSorteadas.push( id );
  localStorage.setItem( 'idStorage', JSON.stringify( idSorteadas ) );
  requisitar( id );
}

document.getElementById( 'campoPesquisar' ).addEventListener( 'focusout', pesquisarPorNumero );
document.getElementById( 'button' ).addEventListener( 'click', pesquisarPorClick );
