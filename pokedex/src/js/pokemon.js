class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objVindoDaApi ) {
    this.nome = objVindoDaApi.name;
    this.thumUrl = objVindoDaApi.sprites.front_default;
    this.id = objVindoDaApi.id;
    this._altura = objVindoDaApi.height;
    this._peso = objVindoDaApi.weight;
    this.tipos = objVindoDaApi.types.map( t => t.type.name )
    this.estatisticas = objVindoDaApi.stats.map( t => `${ t.stat.name } : ${ t.base_stat }%` )
  }

  get altura() {
    return this._altura * 10;
  }

  get peso() {
    return ( this._peso / 10 ).toFixed( 2 );
  }
}
