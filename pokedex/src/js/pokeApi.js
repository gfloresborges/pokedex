class PokeApi {// eslint-disable-line no-unused-vars
  validarID( campoDigitado ) {
    if ( ( campoDigitado >= 1 )
        && ( campoDigitado <= 802 ) ) {
      return true;
    }
    return false;
  }

  buscar( id ) {
    const fazRequisicao = fetch( `https://pokeapi.co/api/v2/pokemon/${ parseInt( id, 10 ) }` )
    return fazRequisicao.then( resultadoEmString => resultadoEmString.json() );
  }

  sortearID() {
    return parseInt( Math.random() * 1000, 10 );
  }
}
